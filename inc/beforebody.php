<!DOCTYPE html>
<html lang="zh-TW">
<head>
<meta charset="utf-8">
<link rel="dns-prefetch" href="//www.google-analytics.com">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="">
<link rel="icon" href="<?=$staticRoot?>/favicon.ico" type="image/x-icon">
<?php
$pageTitle = $hl['HOME'] . $hl['PRODUCT_NAME_SPACER'] . $hl['PRODUCT_NAME'];
    
if( isset($_GET["ipg"]) ){
	switch( $_GET["ipg"] ){
        /*
		case "index":
			$pageTitle = $hl['HOME'];
			break;
        */
		case "test":
		    $pageTitle = $hl['PAGE_TITLE_TEST'];
		    break;
        case "dragSelection":
		    $pageTitle = $hl['PAGE_TITLE_DRAG_SELECTION'];
		    break;
	}
}
?>
<title><?=$pageTitle?></title>

<link rel="stylesheet" href="<?=$staticRoot?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=$staticRoot?>/css/jquery-ui.min.css">
<link rel="stylesheet" href="<?=$staticRoot?>/css/font-awesome.min.css">
<link rel="stylesheet" href="<?=$staticRoot?>/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="<?=$staticRoot?>/css/library.css">


</head>
<body><div class="container-fluid"><!-- START of body > container-fluid -->
