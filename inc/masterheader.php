<header id="hd">
    <h1>Product Name</h1>
    <form class="change-lang" action="" method="get"><select name="hl" class="form-control">
    <?php
        $langs = array(
            "en" => "English",
            "zh-tw" => "Traditional Chinese",
            "zh-cn" => "Simplified Chinese"
        );
        foreach ($langs as $key => $value) {
            echo "<option value=\"{$key}\"";
            if( $key == $_SESSION["hl"] ){
                echo " selected";
            }
            echo ">{$value}</option>";
        }
    ?>
    </select></form>
    <ol class="breadcrumb"><li><a href="?ipg=index&title=<?=$hl["HOME"]?>"><?=$hl["HOME"]?></a></li></ol>
</header>