<div id="jqueryUiTouchPunch">
    <div class="hd"><h1>JQuery UI Touch Punch</h1></div>
    <div class="bd">
        Check out this link <a href="http://touchpunch.furf.com/">http://touchpunch.furf.com/</a>
        <p>

Currently, jQuery UI user interface library does not support the use of touch events in their widgets and interactions. This means that the slick UI you designed and tested in your desktop browser will fail on most, if not all, touch-enabled mobile devices, becuase jQuery UI listens to mouse events—mouseover, mousemove and mouseout—not touch events—touchstart, touchmove and touchend.

That's where jQuery UI Touch Punch comes in. Touch Punch works by using simulated events to map touch events to their mouse event analogs. Simply include the script on your page and your touch events will be turned into their corresponding mouse events to which jQuery UI will respond as expected.
</p>
    </div>
    <div class="ft"></div>
</div>