</div><!-- END of body > container-fluid -->
<div id="toTop"></div>
<div id="selector"></div>


<div id="ajax-loading" class="modal">
    <div class="modal-backdrop fade in"></div>
    <div class="modal">
        <div class="content">
            <img src="pimg/loading.gif">
            <p></p>
        </div>
    </div>
</div>

<div class="modal slide" id="commonModal" tabindex="-1" role="dialog" aria-labelledby="commonModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="commonModalLabel"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cancel" data-dismiss="modal" data-confirm="false"><?=$hl['BUTTON_CANCEL']?></button>
                <button type="button" class="btn btn-default ok" data-dismiss="modal" data-confirm="true"><?=$hl['BUTTON_OK']?></button>
            </div>
        </div>
    </div>
</div>

<div id="commonDialog">
  <p class="msg-body"></p>
</div>

<script src="<?=$staticRoot?>/js/jquery-2.2.3.min.js"></script>
<script src="<?=$staticRoot?>/js/bootstrap.min.js"></script>
<script src="<?=$staticRoot?>/js/jquery-ui.min.js"></script>
<script src="<?=$staticRoot?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?=$staticRoot?>/js/moment-with-locales.min.js"></script>
<script src="<?=$staticRoot?>/js/jquery.history45.min.js"></script>
<script src="<?=$staticRoot?>/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?=$staticRoot?>/js/validator.min.js"></script>
<script src="<?=$staticRoot?>/js/jquery.slimscroll.min.js"></script>
<!--
<script src="<?=$staticRoot?>/js/jquery.iframe-transport.min.js"></script>
<script src="<?=$staticRoot?>/js/jquery.fileupload.min.js"></script>
-->
<script src="<?=$staticRoot?>/js/jquery.ui-contextmenu.min.js"></script>
<script src="<?=$staticRoot?>/js/notify.min.js"></script>
<script src="<?=$staticRoot?>/js/json2.min.js"></script>
<script src="<?=$staticRoot?>/js/isMobile.min.js"></script>
<script src="<?=$staticRoot?>/js/docCookies.min.js"></script>
<script src="<?=$staticRoot?>/js/jquery.s2t.js"></script>
<script>var HL = {<?php
$comma = false;
foreach($hl as $key => $val){
    if( $comma )
        echo ",";
    echo '"' . $key . '":"' . $val . '"';
    $comma = true;
}
?>}</script>
<script src="<?=$staticRoot?>/js/library.js"></script>
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2856605-19', 'auto');
  ga('send', 'pageview');

</script>
-->
</body>
</html>