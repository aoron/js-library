<?php include("../lib/config.php");?><div id="index">
    <div class="hd">
        <div class="modaling">
            <a href="#" data-modal="error">Error Modal</a>
            |
            <a href="#" data-modal="info">Info Modal</a>
            |
            <a href="#" data-modal="warning">Warning Modal</a>
            |
            <a href="#" data-modal="confirm">Confirm Modal</a>
        </div>
        <div class="dialog">
            <a href="#" data-dialog="error">Error Dialog</a>
            |
            <a href="#" data-dialog="info">Info Dialog</a>
            |
            <a href="#" data-dialog="warning">Warning Dialog</a>
            |
            <a href="#" data-dialog="confirm">Confirm Dialog</a>
        </div>
        <a href="#" class="show-ajax-loading">show ajax loading</a>
    </div>
    <div class="bd">
        <a href="?ipg=test&title=Testing page" class="simple-link"><?=$hl["PAGE_TITLE_TEST"]?></a>
        |
        <a href="?ipg=dragSelection&title=Drag Selection" class="simple-link"><?=$hl["PAGE_TITLE_DRAG_SELECTION"]?></a>
        |
        <a href="?ipg=jqueryUiTouchPunch&title=jquery.ui.touch-punch" class="simple-link">jquery.ui.touch-punch</a>
        |
        bootstrap-datetimepicker
        |
        Form validator
        |
        Slim-scroll
        |
        ui-contextmenu
        |
        notify
        |
        json2
        |
        isMobile
        |
        docCookies
        |
        Convert (vise-versas) Traditional Chinese & Simplied Chinese
    </div>
    <div class="ft">
        <div class="paging-al"></div>
        <div class="block-loading"></div>
    </div>
</div>