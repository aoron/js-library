// Preset jQuery ui dialog settings.
$.extend($.ui.dialog.prototype.options, {
    modal: true,
    resizable: false,
    draggable: false
});

$.ajaxSetup({
    "dataType":"json",
    "type": "post",
    "catch": false
});
$(document).ajaxSend(function(e, jqXHR, ajaxOptions){
    JSPN.ajaxContextPool.push(e.target);
    JSPN.xhrPool.push(jqXHR);
}).ajaxComplete(function(e, jqXHR, ajaxOptions){
    // Remove JSPN.ajaxContextPool & JSPN.xhrPool by e.target & jqXHR in the arrays
    var idx = JSPN.xhrPool.indexOf(jqXHR);
    JSPN.xhrPool.splice(idx, 1);
    JSPN.ajaxContextPool.splice(idx, 1);
});

$.ajaxPrefilter('json', function(options, originalOptions, jqXHR) {
    /*
    // append token & company to every post/get ajax requests
    if (originalOptions.type.toUpperCase() == 'POST' || options.type.toUpperCase() == 'POST') {
        if( options.formData ){
            options.data.append("token", docCookies.getItem("token"));
            options.data.append("company", docCookies.getItem("company"));
        }else
            options.data += "&token=" + docCookies.getItem("token") + "&company=" + docCookies.getItem("company");
    }else{
        options.url += ( options.url.indexOf("?") > -1 )?"&":"?";
        options.url += "token=" + docCookies.getItem("token") + "&company=" + docCookies.getItem("company");
    }
    */

    var originalSuccess = options.success;

    if( options.dataTypes[0] === 'json' ){
        options.success = function(json){
            if( ! json ){
                //console.log(options, originalOptions, jqXHR);
                JSPN.Dialog.error( "Invalid JSON!\n" + originalOptions.url );
            }
            switch( json.code ){
                case 1:
                    originalSuccess != null && (originalSuccess(json))
                    break;
                case 588:
                    $.notify( HL.INFO_DELETE_VIOLATION, {className:"info"});
                    break;
                default:
                    if( ! JSPN.Util.withInNoAlertList(options.url) ){ // uploading images api, do not show error here, they show in uploadin modal UI
                        //JSPN.Dialog.error( HL.JSON_ERROR_PROCESSING );
                        $.notify(HL.JSON_ERROR_PROCESSING + "\n" + json.message, {className:"error",autoHideDelay: 10000});
                    }
            }
        };
    }
    
    options.error = function(XMLHttpRequest, textStatus, errorThrown) {
        /*
        // bind to status code to show error, this method does not need json to validate. thus, save network transfering cost & faster.
        if(jqXHR.status === 588){
            $.notify( "Data is being used, can not be deleted!", {className:"info"});
        }
        */
        if( ! JSPN.Util.withInNoAlertList(options.url) && textStatus != "abort" && originalOptions.formData === undefined /*|| originalOptions.formData.microID === undefined*/ ){
            // with microID do not need to show error dialog
            // addFace, setPicture
            $.notify( HL.JSON_ERROR_PROCESSING + "\n" + errorThrown + "\nURL: " + options.url , {className:"error",autoHideDelay: 10000});
        }
    };
});

/* START Init JSPN object */
var JSPN = {
    bsSize: {
        sm: 768,
        md: 992,
        lg: 1200
    },
    productTitle : 'Product Title Name',
    ajaxContextPool : [],
    xhrPool : [],
    pagingSize : 20,
    forceScrollToTop: false
};
/* END Init JSPN object */

/* START JSPN.Switcher */
JSPN.Switcher = {
    daysUnit: function(val){
        switch(parseInt(val)){
            case 1:
                return "天";
                break;
            case 2:
                return "個月";
                break;
            case 3:
                return "年";
                break;
        }
    }
};
/* END JSPN.Switcher */

/* START of JSPN.Util */
JSPN.Util = {
    deferredsRecure: function(aDefs, fCallback){
        /*
        This function takes array of deferreds & waits for all deferreds(failed or resolved) & do the callback function and pass back the aDefs(deferreds array) for checking how many are done success
        */
        $.when.apply($, aDefs).done(function(){
            // Only do this when all are done, fail or resolved
            fCallback(aDefs);
        }).fail(function(oDef){
            // Remove fail def from aDefs
            aDefs.splice(aDefs.indexOf(oDef),1)
            // do it again with all aDefs into $.when
            JSPN.Util.deferredsRecure(aDefs, fCallback);
        });
    },
    withInNoAlertList: function(sURL){
        var withIn = false;
        var list = ["addFace","setPicture"];
        $(list).each(function(i,el){
            if( sURL.endsWith(el) ){
                withIn = true;
                return false;
            }
        });
        return withIn;
    },
    ajaxLoading: function(sShowHide, sMsg){
        var $al = $("#ajax-loading")
        if( sShowHide === "show" ){
            //str !== undefined && str !== null && (str = limit)
            $("p", $al).html(sMsg);
            $al.show();
        }else{
            $("p", $al).empty();
            $al.hide();
        }
    },
    bindAdvancedUpload: function($dropzone){
        if( JSPN.Util.isAdvancedUpload ){
            // Add class has-advanced-upload to all dropzone
            $dropzone.addClass('has-advanced-upload');

            $dropzone.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
            }).on('dragover dragenter', function() {
                $dropzone.addClass('is-dragover');
            }).on('dragleave dragend drop', function() {
                $dropzone.removeClass('is-dragover');
            })
        }
    },
    fileSizeToStr:function(fileSize) {
		var fileSizeStr = "";

		if (fileSize < 0) {
			fileSizeStr = "0 KB";
		} else if (fileSize < 1024*1024) {
			fileSizeStr = Math.round(fileSize/1024) + " KB";
		} else if (fileSize < 1024*1024*1024) {
			fileSizeStr = (fileSize/1024/1024).toFixed(2) + " MB";
		} else if (fileSize < 1024*1024*1024*1024) {
			fileSizeStr = (fileSize/1024/1024/1024).toFixed(2) + " GB";
		} else {
			fileSizeStr = (fileSize/1024/1024/1024/1024).toFixed(2) + " TB";
		}
		return fileSizeStr;
	},
    calWidthToFitImageWindow: function(){
        var ipg = JSPN.Util.getParameterByName("ipg");
        var ww = $(window).width() - JSPN.Util.scrollbarWidth();

        if( ipg === "ts" ){
            var imgWidth = 290;
            var liWidth = 294;
            var itemPerRow = Math.round(ww / liWidth);
            var rest = ww - itemPerRow * liWidth;
            var newLi = rest / itemPerRow + imgWidth - 0.1; // Reduce 0.1 for tollerance
            newLi = Math.round( newLi * 10 ) / 10; //Decimal 1 place
            //JSPN.Util.addCSSRule("#ts .group ol.listing li", "width:" + newLi + "px;")
            var height = (newLi / 16 *9);
            JSPN.Util.addCSSRule("#ts .group ol.listing li .img", "width:" + newLi + "px;height:" + height + "px;")
            JSPN.Util.addCSSRule("#ts .group ol.listing li h5", "width:" + newLi + "px;")
            return {width:newLi,height:height};
        }else if( ipg === "qc" ){
            ww -= 200;
            // Change width & height instead of margin
            var imgWidth = 160;
            //var imgMargin = 10;
            var liWidth = 164;
            var itemPerRow = Math.round(ww / liWidth);
            var rest = ww - itemPerRow * liWidth;
            var newLi = rest / itemPerRow + imgWidth - 0.1; // Reduce 0.1 for tollerance
            newLi = Math.round( newLi * 10 ) / 10; //Decimal 1 place
            //JSPN.Util.addCSSRule("ol.listing li", "width:" + newLi + "px;")
            JSPN.Util.addCSSRule("ol.listing li .img", "width:" + newLi + "px;height:" + newLi + "px;")
            JSPN.Util.addCSSRule("ol.listing li h5", "width:" + newLi + "px;")
            //JSPN.Util.addCSSRule("ol.listing li.add a", "height:" + newLi + "px;")
        }else if( $("ol.round").length > 0 ){
            // Change width & height instead of margin
            var imgWidth = 160;
            //var imgMargin = 15;
            var liWidth = 190 + 4; // 4px of li margin
            var itemPerRow = Math.round(ww / liWidth);
            var rest = ww - itemPerRow * liWidth;
            var newLi = rest / itemPerRow + imgWidth - 0.1; // Reduce 0.1 for tollerance
            newLi = Math.round( newLi * 10 ) / 10; //Decimal 1 place
            //JSPN.Util.addCSSRule("ol.listing.round li", "width:" + newLi + "px;")
            JSPN.Util.addCSSRule("ol.listing.round li .img", "width:" + newLi + "px;height:" + newLi + "px;")
            JSPN.Util.addCSSRule("ol.listing.round li h5", "width:" + newLi + "px;")
            //JSPN.Util.addCSSRule("ol.listing.round li.add a", "height:" + newLi + "px;")
        }else{
            // Change width & height instead of margin
            var imgWidth = 160;
            //var imgMargin = 10;
            var liWidth = 160 + 4; // 4px of li margin
            var itemPerRow = Math.round(ww / liWidth);
            var rest = ww - itemPerRow * liWidth;
            var newLi = rest / itemPerRow + imgWidth - 0.1; // Reduce 0.1 for tollerance
            newLi = Math.round( newLi * 10 ) / 10; //Decimal 1 place
            //JSPN.Util.addCSSRule("ol.listing li", "width:" + newLi + "px;")
            JSPN.Util.addCSSRule("ol.listing li .img", "width:" + newLi + "px;height:" + newLi + "px;")
            JSPN.Util.addCSSRule("ol.listing li h5", "width:" + newLi + "px;")
            //JSPN.Util.addCSSRule("ol.listing li.add a", "height:" + newLi + "px;")
        }
        
        
        
        return itemPerRow;
    },
    scrollbarWidth: function(){
        var scrollDiv = document.createElement("div");
        scrollDiv.id = "scrollbar-measure";
        document.body.appendChild(scrollDiv);
        // Get the scrollbar width
        var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        // Delete the DIV 
        document.body.removeChild(scrollDiv);
        //console.log(scrollbarWidth);
        return scrollbarWidth;
    },
    isAdvancedUpload:function() {
      var div = document.createElement('div');
      return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    },
    clearInput: function(){
        // CLEARABLE INPUT
        function tog(v){return v?'addClass':'removeClass';}
        
        $(document).on('input', '.clearable', function(){
            $(this)[tog(this.value)]('x');
        }).on('mousemove', '.x', function( e ){
            $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');   
        }).on('touchstart click', '.onX', function( ev ){
            ev.preventDefault();
            $(this).removeClass('x onX').val('').change();
            $(this).closest("form").submit();
        });
    },
    containImage: function(maxWidth, maxHeight, imgWidth, imgHeight){
        if( imgWidth > maxWidth ){
            newWidth = maxWidth;
            newHeight = imgHeight * (newWidth / imgWidth);
            return JSPN.Util.containImage(maxWidth, maxHeight, newWidth, newHeight);
        }else if( imgHeight > maxHeight ){
            newHeight = maxHeight;
            newWidth = imgWidth * (newHeight / imgHeight);
            return JSPN.Util.containImage(maxWidth, maxHeight, newWidth, newHeight);
        }else if( imgWidth < maxWidth && imgHeight < maxHeight ){
            //console.log("w h smaller", maxWidth, maxHeight, imgWidth, imgHeight);
            // enlarge 1 by 1 percent to check the sizes, if any side over: stop and use the last sizes
            var lastWidth = imgWidth;
            var lastHeight = imgHeight;
            for( var i = 1; i < 301; i++ ){
                
                var percent = 1+(i/100);
                newWidth = percent * imgWidth;
                newHeight = percent * imgHeight;
                //console.log(i, newWidth, newHeight);
                if( newWidth > maxWidth || newHeight > maxHeight ){
                    //console.log(lastWidth, lastHeight);
                    // use the last sizes
                    //return [lastWidth, lastHeight];
                    break;
                }else{
                    lastWidth = newWidth;
                    lastHeight = newHeight;
                }
            }
            
            return [lastWidth, lastHeight];
        }else{
            return [imgWidth, imgHeight];
        }
    },
    simpleLink: function(){
        // Works with History.js library
        //var $this = $(this);
        var href = $(this).attr("href");
        //var title = $this.attr("title");
        var data = JSPN.Util.getParametersFromString(href);
        History.pushState(data, data.title, href);
    },
    getRandomInt: function(min, max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    showSiteMapVideo: function(){
        $("#ivpModalLabel").text( '網站導覽影片' );
        var imgUrl = 'https://i.ytimg.com/vi/' + JSPN.SiteMapVideoUTID + '/hqdefault.jpg';
        var utid = JSPN.SiteMapVideoUTID;
        var duration = '';
        var $mb = $('#ivpModal').find(".modal-body").empty();

        $mb.html('<div id="vfkm"><div id="utv' + utid + '" class="ivb video" style="height: 505px; background-image: url(' + imgUrl + ')"><div class="play lg" data-utid="' + utid + '"></div><div class="duration">' + duration + '</div></div></div>');

        $('#ivpModal').modal('show');
    },
    constructBreadcrumb: function(bc){
        var html = '<li><a href="?ipg=index&title=' + HL.HOME + '">' + HL.HOME + '</a></li>';
        $(bc).each(function(i,el){
            if( bc.length === i + 1 ){
                html += '<li class="active">' + el.name + '</li>';
            }else{
                html += '<li><a href="' + el.href + '">' + el.name + '</a></li>';
            }
        });
        
        $("#hd .breadcrumb").html(html);
    },
    genGUID: function(){
        function s4(){
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    },
    switchMainNavigation: function(id){
        $('#hd .ft a.on, #hd .hd .mobile-menu a.on').removeClass("on");
        $('#hd .ft a[data-ccid="' + id + '"], #hd .hd .mobile-menu a[data-ccid="' + id + '"]').addClass("on");
    },
    isValidEmail: function(email) {
        return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    },
    isTaiwanMobileNumber: function(val){
        return /^[0]{1}[9]{1}[0-9]{8}$/.test(val)
    },
    secondTohhmmss: function(secs) {
        var pad = function(str) {
            return ("0"+str).slice(-2);
        }
        
        var minutes = Math.floor(secs / 60);
        secs = parseInt( secs % 60 );
        var hours = Math.floor(minutes/60);
        minutes = minutes%60;
        
        return pad(hours)+":"+pad(minutes)+":"+pad(secs);
    },
    secondTohhmmssFilter: function(secs) {
        var pad = function(str) {
            return ("0"+str).slice(-2);
        }
        
        var minutes = Math.floor(secs / 60);
        secs = parseInt( secs % 60 );
        var hours = Math.floor(minutes/60);
        minutes = minutes%60;
        
        var time = "";
        if( hours )
            time += pad(hours)+":";
        
        return time + pad(minutes) + ":" + pad(secs);
    },
    stripHtmlTags: function(html){
       var tmp = document.createElement("DIV");
       tmp.innerHTML = html;
       return tmp.textContent || tmp.innerText || "";
    },
    cancelUploadingActiveAjaxes:function(){
        $(JSPN.uploadingXhrPool).each(function(i,el){
            //console.log(el);
            el.abort();
        });
        JSPN.uploadingXhrPool = [];
    },
    cancelActiveAjaxes:function(e){
        //console.log("cancelActiveAjaxes...", JSPN.xhrPool.length, JSPN.ajaxContextPool.length);
        $(JSPN.xhrPool).each(function(i,el){
            //console.log(el);
            // abort all XHR ajax call
            el.abort();
        });
        JSPN.xhrPool = [];
        JSPN.ajaxContextPool = [];
        JSPN.Util.cancelUploadingActiveAjaxes();
    },
    getImgSize:function(imgSrc) {
        var newImg = new Image();

        newImg.onload = function() {
            var height = newImg.height;
            var width = newImg.width;
            //alert ('The image size is '+width+'*'+height);
            delete newImg;
            return [width,height];
        }

        newImg.src = imgSrc; // this must be done AFTER setting onload
    },
    getFileName:function(){
        var url = window.location.pathname;
        var filename = url.substring(url.lastIndexOf('/')+1);
        return filename.split(".")[0];
    },
    constructPagination:function($dModule, pages, currentPage){
        $('ul.pagination',$dModule).empty();

        var html = '<li class="prev';
        if( currentPage == 1 )
            html += ' disabled';
        html += '"><a href="#">&laquo;</a></li>';
        for(var i=1;i<=pages;i++){
            html += '<li';
            if( currentPage == i )
                html += ' class="disabled"';
            html += '><a href="#">' + i + '</a></li>';
        }
        html += '<li class="next';
        if( currentPage == pages )
            html += ' disabled';
        html += '"><a href="#">&raquo;</a></li>';

        $('ul.pagination',$dModule).empty().html(html);
    },
    isElementInViewport: function(el){ 
        if (typeof jQuery === "function" && el instanceof jQuery)
            el = el[0];
        
        if( el === undefined ) return false;
        
        var rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );
    },
    percentElementInViewport: function(el){// top & bottom calc only, no left & right 
        
        //special bonus for those using jQuery
        if (typeof jQuery === "function" && el instanceof jQuery)
            el = el[0];
        
        if( el === undefined ) return false;

        var rect = el.getBoundingClientRect();
        //console.log(rect.top , rect.bottom, window.innerHeight);
        var height = rect.bottom - rect.top;
        //console.log(height);
        //console.log(window.innerHeight - rect.top);
        //console.log("-----------");
        var x = ( rect.top > 0 )?window.innerHeight - rect.top: rect.top + height;
        return x / height;
    },
    elementInViewport:function(el,range) {
        var wih = window.innerHeight;
        var wiw = window.innerWidth;

        if( range !== undefined ){
            if( range.height )
                wih += range.height;

            if( range.width )
                wiw += range.width;
        }

        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;

        while(el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        return (
            top < (window.pageYOffset + wih) &&
            left < (window.pageXOffset + wiw) &&
            (top + height) > window.pageYOffset &&
            (left + width) > window.pageXOffset
        );
    },
    elementAllInViewport:function(el,range) {
        var wih = window.innerHeight;
        var wiw = window.innerWidth;

        if( range !== undefined ){
            if( range.height )
                wih += range.height;

            if( range.width )
                wiw += range.width;
        }

        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;

        while(el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        return (
            top >= window.pageYOffset &&
            left >= window.pageXOffset &&
            (top + height) <= (window.pageYOffset + wih) &&
            (left + width) <= (window.pageXOffset + wiw)
        );
    },
    elementAllInElement:function($el, $ctn, range){
        //console.log($el, $ctn, range);
        var ctnH = $ctn.height();
        var ctnW = $ctn.width();
        
        if( range !== undefined ){
            if( range.height )
                ctnH += range.height;

            if( range.width )
                ctnW += range.width;
        }
        
        var $os = $el.position();
        var positionTop = $os.top + $ctn.scrollTop();
        var positionLeft = $os.left + $ctn.scrollLeft();
        var width = $el.width();
        var height = $el.height();

        /*
        console.log( positionTop, ">=" , $ctn.scrollTop() );
        console.log( positionLeft , ">=", $ctn.scrollLeft() );
        console.log( (positionTop + height), "<=" , ($ctn.scrollTop() + ctnH) );
        console.log( (positionLeft + width), "<=" , ($ctn.scrollLeft() + ctnW) );
        */
        
        return (
            positionTop >= $ctn.scrollTop() &&
            positionLeft >= $ctn.scrollLeft() &&
            (positionTop + height) <= ($ctn.scrollTop() + ctnH) &&
            (positionLeft + width) <= ($ctn.scrollLeft() + ctnW)
        );
    },
    addCSSRule:function(selector, rules, index) {
        index === undefined && (index = 0)
        // Create the <style> tag
        var style = document.createElement("style");

        // Add a media (and/or media query) here if you'd like!
        // style.setAttribute("media", "screen")
        // style.setAttribute("media", "@media only screen and (max-width : 1024px)")

        // WebKit hack :(
        style.appendChild(document.createTextNode(""));

        // Add the <style> element to the page
        document.head.appendChild(style);

        var sheet = style.sheet;

        if ( sheet.insertRule ) {
            sheet.insertRule(selector + "{" + rules + "}", index);
        } else {
            sheet.addRule(selector, rules, index);
        }
    },
    getParameterByName:function(name) {
        /*
        // Get parameter from History will need decodeURIComponent. so, I just use below
        var dData = History.getState().data[name];
        if( typeof dData !== "undefined" )
            return dData
        */

        // ie 9 and below using hash
        var para = ( location.search === "" )?location.hash:location.search;
        /*
        if( typeof name === "undefined" )
            return null;
        
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(location.search);
        return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
        */
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(para)||[,""])[1].replace(/\+/g, '%20'))||null;
    },
    getParameterFromString:function(str,name){
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(str)||[,""])[1].replace(/\+/g, '%20'))||null;
    },
    getParametersFromString:function(str){
        var qs = {};
        str.replace( new RegExp("([^?=&]+)(=([^&]*))?", "g"), function($0, $1, $2, $3) { qs[$1] = $3; } );
        return qs;
    },
    bindDragSelection:function($scope, fClickEventCB){
        
        var selecting = false;
        var doNotClick = false; // used for $scope click event, to not remove all selected during 100 millisec after selecting. or selected will be removed after area selecting ends(calling $scope click event)
		var sx; // Start X where mousedown occur
		var sy; // Start Y where mousedown occur
		var selLeft;
		var selTop;
		var selRight;
		var selBottom;
		var selWidth;
		var selHeight;
		var dModuleLeft = 0; // Left space in px of this module
        var dModuleTop = 0;//$(".bd", $dModule).position().top;
		var thisPageSelected = [];
        
        var $lastClickedItem;
        
        var $selector = $("<div>").addClass("selector").appendTo($scope);
        $scope.css("position","relative"); // position:relative; makes getBoundingClientRect it's parent 
        
        var scopeRect = $scope[0].getBoundingClientRect();
        
        function intersectRect(r1, r2){
            return !(r2.left > r1.right || r2.right < r1.left || r2.top > r1.bottom ||r2.bottom < r1.top);
        }

        var $toBind = $.merge($(".selector", $scope), $scope);
		$toBind.on("mousedown mouseup mousemove",function(e){
			if( e.type == "mousedown" /*&& $(e.target).hasClass("listing")*/ ){
                //console.log(e.clientX, e.clientY,$(this).scrollTop());
				//dModuleLeft = $(dModule).position().left;

                
                sx = e.clientX - scopeRect.left;// - dModuleLeft;
				sy = e.clientY - scopeRect.top + $scope.scrollTop();// - dModuleTop;
                //console.log(sx, sy);
				$selector.css({left:sx,top:sy,width:0,height:0});
				selecting = doNotClick = true;
				// Keep seleting without remove already selected assets
				if(e.ctrlKey || e.shiftKey){
					// Get all already selected assets at this page
					$('li.normalDrag', $scope).each(function(i,el){
						thisPageSelected.push($(el).attr("guid"));
					});
				}else{
                    $('li.normalDrag', $scope).removeClass("normalDrag");
                }
			}else if( e.type == "mouseup" ){
                //console.log(e.type)
				$selector.hide();
				thisPageSelected = [];
                selecting = false;
                
                
                setTimeout(function(){
                    doNotClick = false;
                },100)
			}else if( e.type == "mousemove" && selecting ){
                //console.log(e.type)
                //console.log("mousemove");
				var scrollTop = $scope.scrollTop();//$(this).scrollTop();
				var x = e.clientX - scopeRect.left;// - dModuleLeft;
				var y = e.clientY - scopeRect.top + scrollTop;// - dModuleTop;// + scrollTop;
                
                if( x > sx ){
                    selLeft = sx;
                    selWidth = x - sx + 5;
                }else{
                    selLeft = x - 5;
                    selWidth = sx - x;
                }
                
                if( y > sy ){
                    selTop = sy;
                    selHeight = y - sy + 5;
                }else{
                    selTop = y - 5;
                    selHeight = sy - y;
                }
				
				$selector.css({left:selLeft,top:selTop,width:selWidth,height:selHeight,display:"block"});
                selTop += scrollTop;
				selRight = selLeft + selWidth;
				selBottom = selTop + selHeight;
                
                //console.log(selLeft, selTop, selRight, selBottom);
                
                var selectorRect = $selector[0].getBoundingClientRect();
                //console.log(rect);
                
                

				$('li:not(.add)', $scope).each(function(i,el){
					$this = $(this);
                    var addNormalDrag = intersectRect(selectorRect, el.getBoundingClientRect());
					
					if( e.ctrlKey ){
						// Check within array thisPageSelected, true -> do opposite 
						addNormalDrag = ( thisPageSelected.indexOf($this.attr("guid")) < 0 )?addNormalDrag:!addNormalDrag;
					}else if( e.shiftKey ){
						var idx = thisPageSelected.indexOf($this.attr("guid"));
						if( idx >= 0 ){
							if( addNormalDrag ){
								// Remove from array thisPageSelected
								thisPageSelected.splice(idx,1);
							}
							addNormalDrag = ( idx < 0 )?addNormalDrag:true;
						}
					}
					
					if( addNormalDrag ){
						$this.addClass("normalDrag");
					}else{
						$this.removeClass("normalDrag");
					}
				});
                
                var $firstOne = $("li.normalDrag:first", $scope);
                if( $firstOne.length > 0 )
                    $lastClickedItem = $firstOne;
			}
		});
        
        //document.oncontextmenu = function() {return false;};
        $scope.on("mousedown click", "li:not(.add)", function(e){
            //console.log("e.type",e.type)
            e.preventDefault();
            e.stopPropagation();
            
            if( e.type === "mousedown" )
                return; // mousedown do not propagate to $scope level, or it will deselect the li
            
            var ct = e.delegateTarget;
            //console.log(ct, e.target, e.relatedTarget, e);
            var $this = $(this);
            if( e.shiftKey ){
                var lciIdx = $("li", ct).index($lastClickedItem);
                var thisIdx = $("li", ct).index($this);
                var startIdx = (lciIdx < thisIdx)?lciIdx:thisIdx;
                var endIdx = (lciIdx > thisIdx)?lciIdx:thisIdx;
                ++endIdx;

                $("li.normalDrag", ct).removeClass("normalDrag");

                $("li", ct).slice(startIdx, endIdx).addClass("normalDrag");
            }else{
                $this.toggleClass("normalDrag", !$this.hasClass("normalDrag"));
                $lastClickedItem = $this;
            }
            
            fClickEventCB && ( fClickEventCB.call(this) )
        })
        
        $scope.click(function(e){
            if(doNotClick) return;
            // Remove all selected
            $("li.normalDrag", e.delegateTarget).removeClass("normalDrag");
        });
        
    },
    checkPassword:function( str ){
		// at least one number, one non-word, one lowercase and one uppercase letter
        // at least six characters
        var re = /(?=.*\d)(?=.*\W)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
        return re.test(str);
	},
    toTime:function(t){
        // Second to time xx day xx hour xx min xx sec
        var str = "";
        
        var day = Math.floor( t / 60 / 60 / 24 );
        if( day ){
            str += day;
            str += (day > 1)?" days ":" day ";
            t -= (60 * 60 * 24) * day;
        }
        var hour = Math.floor( t / 60 / 60 );
        if( hour ){
            str += hour;
            str += (hour > 1)?" hours ":" hour ";
            t -= ((60 * 60) * hour);
        }
        var min = Math.floor( t / 60 );
        if( min ){
            str += min
            str += (min > 1)?" mins ":" min ";
            t -= (60) * min;
        }
        var sec = t;
        str += sec;
        str += (sec > 1)?" secs":" sec";

		return str;
    },
    toDigitalTime:function(t){
        var time = parseFloat(t);
        // Second to digital time 00:00:00
        var str = "";
        
        var hour = Math.floor( time / 60 / 60 );
        time -= ((60 * 60) * hour);
        str += ("0" + hour).slice(-2) + ":";

        var min = Math.floor( time / 60 );
        time -= (60) * min;
        str += ("0" + min).slice(-2) + ":";

        // Second
        var sec = Math.round(time);
        str += ("0" + sec).slice(-2);

		return str;
    },
    preloadImages:function(imgArr){
		var images = new Array();
		var l = imgArr.length;
		for(var i=0;i<l;i++){
			images[i] = new Image();
			images[i].src = imgArr[i];
		}
	},
	urldecode:function(str) {
		return decodeURIComponent((str+'').replace(/\+/g, '%20'));
	},
	isCanvasSupported:function(){ // Return true if canvas is supported
		return !!document.createElement("canvas").getContext;
	}
}
/* END of JSPN.Util */

/* START of JSPN.Dialog - JQuery-UI Dialog */
JSPN.Dialog = {
    common: function(sType, sMsg, fCallBack, iWidth){
        var $cDialog = $("#commonDialog");
        var w = ( iWidth )?iWidth:300;
        $("p.msg-body",$cDialog).html(sMsg);
        var title;
        var dialogButtons = {};
        var closeFunc = function(e,ui){ console.log(852); fCallBack && (fCallBack()) };
        var closeOnEscape = true;

        dialogButtons[HL.BUTTON_OK] = function(e) {
            e.stopPropagation();
            $(this).dialog('close');
        };
        switch(sType){
            case "info":
                title = HL.INFO_MODAL_TITLE;
                break;
            case "warning":
                title = HL.WARNING_MODAL_TITLE;
                break;
            case "error":
                title = HL.ERROR_MODAL_TITLE;
                break;
            case "confirm":
                title = HL.CONFIRM_MODAL_TITLE;

                dialogButtons[HL.BUTTON_OK] = function() {
                    fCallBack && (fCallBack(true))
                    $(this).dialog("close");
                };
                dialogButtons[HL.BUTTON_CANCEL] = function() {
                    fCallBack && (fCallBack(false))
                    $(this).dialog("close");
                };

                closeFunc = null;
                closeOnEscape = false;
                break;
        }

        if( $cDialog.is(':data(dialog)') ){
            $cDialog.dialog("option", {
                width: w,
                title: title,
                buttons: dialogButtons,
                close: closeFunc,
                closeOnEscape: closeOnEscape
            }).dialog('open');
        }else{
            $cDialog.dialog({
                title: title,
                closeOnEscape: closeOnEscape,
                autoOpen: true,
                modal: true,
                width : w,
                height: 300,
                buttons: dialogButtons,
                beforeClose: function(e){
                    e.stopPropagation();
                },
                close: closeFunc,
                open: function(event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                }
            });
        }
    }
}
/* END of JSPN.Dialog */
/* START JSPN.Modal */
JSPN.Modal = {
    common: function(sType, sMsg, fCallBack){
        var $modal = $("#commonModal")
        $(".modal-footer button", $modal).off("click");
        $modal.off("hidden.bs.modal");
        $(".modal-body", $modal).html( sMsg );
        var title;
        var backdrop = "static";
        var keyboard = true;
        
        var headerClass;

        switch(sType){
            case "info":
                headerClass = "info";
                title = HL.INFO_MODAL_TITLE;
                $(".modal-footer button.cancel", $modal).hide();
                break;
            case "warning":
                headerClass = "warning";
                title = HL.WARNING_MODAL_TITLE;
                $(".modal-footer button.cancel", $modal).hide();
                break;
            case "error":
                headerClass = "danger";
                title = HL.ERROR_MODAL_TITLE;
                $(".modal-footer button.cancel", $modal).hide();
                break;
            case "confirm":
                headerClass = "primary";
                title = HL.CONFIRM_MODAL_TITLE;
                $(".modal-footer button.cancel", $modal).show();
                $(".modal-footer button", $modal).on("click", function(e){
                    console.log(222999);
                    fCallBack && ( fCallBack($(this).data("confirm")) )
                });

                
                keyboard = false;
                break;
        }
        
        $(".modal-header", $modal).attr("class", "modal-header modal-header-" + headerClass + " " + sType);

        $(".modal-title", $modal).html(title);

        if( $modal.is(':data(bs.modal)') ){
            var data = $modal.data("bs.modal");
            data.options.backdrop = backdrop;
            data.options.keyboard = keyboard;
            data.show();
        }else{
            $modal.modal({
                backdrop: backdrop,
                keyboard: keyboard
            })
        }

        $modal.on("hidden.bs.modal", function(e){
            if( sType !== "confirm" ){
                console.log(111888);
                fCallBack && ( fCallBack() )
            }
        });
    }
}
/* END JSPN.Modal */

/* START JSPN.Page */
JSPN.Page = {};
(function(){
    JSPN.Page.Index = function(){
        this.pageJqXHR = $.get('inc/index.php', function(data){
            JSPN.Util.constructBreadcrumb();
            $('#bd').html(data);
            this.$dModule = $("#index");
            
            $(".modaling a", this.$dModule).click(function(e){
                e.preventDefault();
                var modal = $(this).data("modal");
                JSPN.Modal.common(modal, "testing....", function(tf){ console.log("called backed!!!", tf); });
            });
            $(".dialog a", this.$dModule).click(function(e){
                e.preventDefault();
                var dialog = $(this).data("dialog");
                JSPN.Dialog.common(dialog, "testing....", function(tf){ console.log("called backed!!!", tf); });
            });
            
            $("a.show-ajax-loading", this.$dModule).click(function(e){
                e.preventDefault();
                JSPN.Util.ajaxLoading("show", "with some message<br>To hide please use JSPN.Util.ajaxLoading(\"hide\")");
            });
            
            $("a.simple-link", this.$dModule).click(function(e){
                e.preventDefault();
                JSPN.Util.simpleLink.call(this);
            });
        },"html");
    }
    
    JSPN.Page.Index.prototype = {
        //
    }
})();

(function(){
    JSPN.Page.DragSelection = function(){
        JSPN.Util.constructBreadcrumb([{"name":HL.PAGE_TITLE_DRAG_SELECTION,"href":"?ipg=dragSelection&title=" + HL.PAGE_TITLE_DRAG_SELECTION}]);
        
        this.pageJqXHR = $.get('inc/dragSelection.php', function(data){
            
            $('#bd').html(data);
            this.$dModule = $("#drag-selection");
            this.$db = $(".bd", this.$dModule);
            
            this.$ul = $(".bd ul", this.$dModule);
            
            for(var i = 0; i < 60; i++){
                var html = '<li>' + i + '</li>';
                this.$ul.append(html);
            }
            
            JSPN.Util.bindDragSelection(this.$db, function(){
                // li clicked callback, bring li as this in
                console.log("xx",this);
            })
        },"html");
    }
    
    JSPN.Page.DragSelection.prototype = {
        //
    }
})();

(function(){
    JSPN.Page.JQueryUiTouchPunch = function(){
        JSPN.Util.constructBreadcrumb([{"name":HL.PAGE_TITLE_DRAG_SELECTION,"href":"?ipg=dragSelection&title=" + HL.PAGE_TITLE_DRAG_SELECTION}]);
        
        this.pageJqXHR = $.get('inc/jqueryUiTouchPunch.php', function(data){
            
            $('#bd').html(data);
            this.$dModule = $("#jqueryUiTouchPunch");
            this.$db = $(".bd", this.$dModule);
        },"html");
    }
    
    JSPN.Page.JQueryUiTouchPunch.prototype = {
        //
    }
})();

(function(){
    JSPN.Page.Test = function(){
        this.pageJqXHR = $.get('inc/test.php', function(data){
            JSPN.Util.constructBreadcrumb([{"name":HL.PAGE_TITLE_TEST,"href":"?ipg=test&title=" + HL.PAGE_TITLE_TEST}]);
            $('#bd').html(data);
            this.$dModule = $("#test");
            
            $("a.simple-link", this.$dModule).click(function(e){
                e.preventDefault();
                JSPN.Util.simpleLink.call(this);
            });
        },"html");
    }
    
    JSPN.Page.Test.prototype = {
        //
    }
})();
/* END JSPN.Page */

/* START JSPN.Common */
JSPN.Common = {}
JSPN.Common.MasterHeader = {};
(function(){
    JSPN.Common.MasterHeader = function(dModule){
        this.$dModule = $(dModule);
        
        $(".change-lang select", this.$dModule).change(function(e){
            var hl = $(this).val();
            var oPar = {};
            if( location.search ){// filter repeative object name
                oPar = JSPN.Util.getParametersFromString(location.search);
            }
            oPar.hl = hl;
            
            var url = "";
            for(var key in oPar){
                url += ( url )?"&":"?";
                url += key + "=" + oPar[key];
            }

            location.hash && (url += location.hash)

            location.href = url;
        });
        
        $(".breadcrumb", this.$dModule).on("click", "a", function(e){
            e.preventDefault();
            JSPN.Util.simpleLink.call(this);
        });
    }
    
    JSPN.Common.MasterHeader.prototype = {
        //
    }
})();

JSPN.Common.MasterFooter = {};
(function(){
    JSPN.Common.MasterFooter = function(dModule){
        this.$dModule = $(dModule);
    }
    
    JSPN.Common.MasterFooter.prototype = {
        //
    }
})();

JSPN.Common.ToTop = {};
(function(){
    JSPN.Common.ToTop = function(dModule){
        this.$tt = $(dModule);
        this.animating = false;

        this.$tt.on("click", function(e){
            //$(window).animate({scrollTop:0}, 'fast');
            $("html, body").animate({scrollTop:0}, 'fast');
        });
    }
    
    JSPN.Common.ToTop.prototype = {
        scrolling: function(e){
            if( $(window).scrollTop() >= 520 ){
                if( ! this.$tt.is(":visible") && ! this.animating ){
                    this.$tt.fadeToggle({
                        start:function(){ this.animating = true; },
                        complete:function(){ this.animating = false; }
                    });
                }
            }else{
                if( this.$tt.is(":visible") && ! this.animating ){
                    this.$tt.fadeToggle({
                        start:function(){ this.animating = true; },
                        complete:function(){ this.animating = false; }
                    });
                }
            }
        }
    }
})();
/* END JSPN.Common */

/* START JSPN.Module */
JSPN.Module = {
    masterHeader: function(dModule){
        JSPNCMH = new JSPN.Common.MasterHeader(dModule);
    },
    masterFooter: function(dModule){
        JSPNCMF = new JSPN.Common.MasterFooter(dModule);
    },
    toTop: function(dModule){
        JSPNCTT = new JSPN.Common.ToTop(dModule);
    }
}
/* END JSPN.Module */


$(document).ready(function(){
    if( top != self ){
        alert('此網站不可嵌入於其他網站中');
        //top.location = 'http://video.ey.gov.tw';
        return;
    }
    
    var doWhileExist = function(sModuleId,oFunction){
        if(document.getElementById(sModuleId)){oFunction(document.getElementById(sModuleId));}
    };

    doWhileExist('hd', JSPN.Module.masterHeader);
    doWhileExist('ft', JSPN.Module.masterFooter);
    doWhileExist('toTop', JSPN.Module.toTop);
    
    // Init
    var ipg = JSPN.Util.getParameterByName("ipg");
    //console.log("ipg = ", ipg)
    ipg || (ipg = "index");
    
    switch( ipg ){
        case "index":
            JSPNPIDX = new JSPN.Page.Index;
            break;
        case "dragSelection":
            JSPNPDS = new JSPN.Page.DragSelection;
            break;
        case "jqueryUiTouchPunch":
            JSPNPJQUITP = new JSPN.Page.JQueryUiTouchPunch;
            break;
        case "test":
            JSPNTEST = new JSPN.Page.Test;
            break;
    }
});

//JSPN.noChangeScrolls = ["article_detail", "advanceSearch"];

// Bind to State Change Event for jquery.history.js
History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
    if( top != self ){
        alert('此網站不可嵌入於其他網站中');
        //top.location = 'http://video.ey.gov.tw';
        return;
    }
    
    // cancel/abort all active ajaxes before going into other page
    JSPN.Util.cancelActiveAjaxes();// Same page do not need to do this
    
    // Log the State
    var State = History.getState(); // Note: We are using History.getState() instead of event.state
    //History.log('statechange:', State.data, State.title, State.url);
    
    var ipg = State.data.ipg;
    ipg || (ipg = "index");

    if( JSPN.forceScrollToTop )
        $(window).scrollTop(0);
    
    switch( ipg ){
        case "index":
            JSPNPIDX = new JSPN.Page.Index;
            break;
        case "dragSelection":
            JSPNPDS = new JSPN.Page.DragSelection;
            break;
        case "jqueryUiTouchPunch":
            JSPNPJQUITP = new JSPN.Page.JQueryUiTouchPunch;
            break;
        case "test":
            JSPNTEST = new JSPN.Page.Test;
            break;
    }
    

   
    /*
    // Google Analytics
    var page = State.cleanUrl;
    var n = page.indexOf("://");
    page = page.substr(n+3);
    n = page.indexOf("/");
    page = page.substr(n);
    //console.log("ga()" + page, State.title);
    ga('set', { page: page, title: State.title });
    ga('send', 'pageview');
    */
});

$(window).scroll(function(e){
    if( document.getElementById("toTop") ) JSPNCTT.scrolling(e);
});


window.onbeforeunload = function(e) {
    JSPN.Util.cancelActiveAjaxes();
    
    //return 'Please press the Logout button to logout.';
    var ipg = JSPN.Util.getParameterByName("ipg");
    if( ipg === "ts" ){
        if( $("#ts .group ol.listing li").length > 0 )
            return HL.TAGGING_SERVICE_UNDONE;
    }else if( ipg === "qc" && JSPNPQC.undone() ){
        return HL.QUALITY_CONTROL_UNDONE;
    }
};