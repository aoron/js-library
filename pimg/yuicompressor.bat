@echo off

java -jar yuicompressor-2.4.8.jar js/docCookies.js -o js/docCookies.min.js --charset utf-8
echo "docCookies.min.js done!"

java -jar yuicompressor-2.4.8.jar js/isMobile.js -o js/isMobile.min.js --charset utf-8
echo "isMobile.min.js done!"

java -jar yuicompressor-2.4.8.jar js/library.js -o js/library.min.js --charset utf-8
echo "gey.min.js done!"

java -jar yuicompressor-2.4.8.jar css/library.css -o css/library.min.css --charset utf-8
echo "gey.min.css done!"

echo "Press Enter to exit."
pause>nul