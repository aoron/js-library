<?php
$hl = array();

$hl['STATIC_ROOT'] = $staticRoot;

$hl['COMMON_ERROR_PROCESSING'] = '系统错误，请稍候再试！';
$hl['JSON_ERROR_PROCESSING'] = '系统错误，请稍候再试！';

$hl['INFO_MODAL_TITLE'] = '资讯';
$hl['WARNING_MODAL_TITLE'] = '警告';
$hl['ERROR_MODAL_TITLE'] = '错误';
$hl['CONFIRM_MODAL_TITLE'] = '确认';

$hl['INFO_DELETE_VIOLATION'] = '资料使用中无法删除！';

$hl['NOTHING_SELECTED_TO_DELETE_INFO'] = '请点选最少一项来进行删除！';
$hl['CONFIRM_DELETE_ITEMS'] = '确认要删除选取的项目吗？';
$hl['CONFIRM_DELETE_ITEM'] = '确认要删除选取的项目吗？';
$hl['NOTHING_SELECTED_FOR_ACTION_INFO'] = '请点选最少一项来继续！';

$hl['FILE_TYPE_NOT_ACCEPTED'] = '此档案格式不允许！';
$hl['FILE_TOO_BIG'] = '档案过大！';
$hl['UPLOAD_SUCCESS'] = '上传成功';

$hl['BUTTON_OK'] = '提交';
$hl['BUTTON_SUBMIT'] = '提交';
$hl['BUTTON_CANCEL'] = '取消';
$hl['BUTTON_CLOSE'] = '关闭';
$hl['BUTTON_TRASH'] = '移除';
$hl['BUTTON_INFO'] = '资讯';
$hl['BUTTON_UPLOAD'] = '上传';
$hl['BUTTON_TRY_AGAIN'] = '再试一次';
$hl['BUTTON_ABORT'] = '退出';
$hl['BUTTON_ADD'] = '新增';
$hl['BUTTON_SAVE'] = '储存';
$hl['BUTTON_DONE'] = '结束';
$hl['BUTTON_NEW'] = '新增';

$hl['LABEL_SELECT_ALL'] = '全选';

$hl['CHOOSE_FILE'] = '选择一个档案';
$hl['CHOOSE_FILES'] = '选择档案';
$hl['OR_DRAG_IT_HERE'] = '或拖拉到此处';
$hl['OR_DRAG_THEM_HERE'] = '或拖拉到此处';



$hl['PRODUCT_NAME'] = 'JS-图书馆';
$hl['PRODUCT_NAME_SPACER'] = ' | ';
$hl['HOME'] = '首页';
$hl['PAGE_TITLE_TEST'] = '测试页';
$hl['PAGE_TITLE_DRAG_SELECTION'] = '拖拉多选';
/*
var str = "";
for(var key in HL){
  str += "$hl['" + key + "'] = '" + $.t2s(HL[key]) + "';\n";
}
console.log(str);
*/
?>