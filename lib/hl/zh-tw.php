<?php
$hl = array();

$hl['STATIC_ROOT'] = $staticRoot;

$hl['COMMON_ERROR_PROCESSING'] = '系統錯誤，請稍候再試！';
$hl['JSON_ERROR_PROCESSING'] = '系統錯誤，請稍候再試！';

$hl['INFO_MODAL_TITLE'] = '資訊';
$hl['WARNING_MODAL_TITLE'] = '警告';
$hl['ERROR_MODAL_TITLE'] = '錯誤';
$hl['CONFIRM_MODAL_TITLE'] = '確認';

$hl['INFO_DELETE_VIOLATION'] = '資料使用中無法刪除！';

$hl['NOTHING_SELECTED_TO_DELETE_INFO'] = '請點選最少一項來進行刪除！';
$hl['CONFIRM_DELETE_ITEMS'] = '確認要刪除選取的項目嗎？';
$hl['CONFIRM_DELETE_ITEM'] = '確認要刪除選取的項目嗎？';
$hl['NOTHING_SELECTED_FOR_ACTION_INFO'] = '請點選最少一項來繼續！';

$hl['FILE_TYPE_NOT_ACCEPTED'] = '此檔案格式不允許！';
$hl['FILE_TOO_BIG'] = '檔案過大！';
$hl['UPLOAD_SUCCESS'] = '上傳成功';

$hl['BUTTON_OK'] = '提交';
$hl['BUTTON_SUBMIT'] = '提交';
$hl['BUTTON_CANCEL'] = '取消';
$hl['BUTTON_CLOSE'] = '關閉';
$hl['BUTTON_TRASH'] = '移除';
$hl['BUTTON_INFO'] = '資訊';
$hl['BUTTON_UPLOAD'] = '上傳';
$hl['BUTTON_TRY_AGAIN'] = '再試一次';
$hl['BUTTON_ABORT'] = '退出';
$hl['BUTTON_ADD'] = '新增';
$hl['BUTTON_SAVE'] = '儲存';
$hl['BUTTON_DONE'] = '結束';
$hl['BUTTON_NEW'] = '新增';

$hl['LABEL_SELECT_ALL'] = '全選';

$hl['CHOOSE_FILE'] = '選擇一個檔案';
$hl['CHOOSE_FILES'] = '選擇檔案';
$hl['OR_DRAG_IT_HERE'] = '或拖拉到此處';
$hl['OR_DRAG_THEM_HERE'] = '或拖拉到此處';



$hl['PRODUCT_NAME'] = 'JS-圖書館';
$hl['PRODUCT_NAME_SPACER'] = ' | ';
$hl['HOME'] = '首頁';
$hl['PAGE_TITLE_TEST'] = '測試頁';
$hl['PAGE_TITLE_DRAG_SELECTION'] = '拖拉多選';
?>