<?php
$hl = array();

$hl['STATIC_ROOT'] = $staticRoot;

$hl['COMMON_ERROR_PROCESSING'] = 'An error occurred while attempting to process data, please try again later!';
$hl['JSON_ERROR_PROCESSING'] = 'An error occurred while attempting to process data, please try again later!';

$hl['INFO_MODAL_TITLE'] = 'Information';
$hl['WARNING_MODAL_TITLE'] = 'Warning';
$hl['ERROR_MODAL_TITLE'] = 'Error';
$hl['CONFIRM_MODAL_TITLE'] = 'Confirmation';

$hl['INFO_DELETE_VIOLATION'] = 'Data is being used, can not be deleted!';

$hl['NOTHING_SELECTED_TO_DELETE_INFO'] = 'Please select at least one item';
$hl['CONFIRM_DELETE_ITEMS'] = 'Are you sure to delete selected items?';
$hl['CONFIRM_DELETE_ITEM'] = 'Are you sure to delete selected item?';
$hl['NOTHING_SELECTED_FOR_ACTION_INFO'] = 'Please select as least one item to continue!';

$hl['FILE_TYPE_NOT_ACCEPTED'] = 'This file is invalid and will be removed';
$hl['FILE_TOO_BIG'] = 'The file is over the 10MB limit';
$hl['UPLOAD_SUCCESS'] = 'The upload was successful';

$hl['BUTTON_OK'] = 'OK';
$hl['BUTTON_SUBMIT'] = 'Submit';
$hl['BUTTON_CANCEL'] = 'Cancel';
$hl['BUTTON_CLOSE'] = 'Close';
$hl['BUTTON_TRASH'] = 'Trash';
$hl['BUTTON_INFO'] = 'Information';
$hl['BUTTON_UPLOAD'] = 'Upload';
$hl['BUTTON_TRY_AGAIN'] = 'Try again';
$hl['BUTTON_ABORT'] = 'Abort';
$hl['BUTTON_ADD'] = 'Add';
$hl['BUTTON_SAVE'] = 'Save';
$hl['BUTTON_DONE'] = 'Done';
$hl['BUTTON_NEW'] = 'New';

$hl['LABEL_SELECT_ALL'] = 'Select all';

$hl['CHOOSE_FILE'] = 'Browse for a file';
$hl['CHOOSE_FILES'] = 'Browse for files';
$hl['OR_DRAG_IT_HERE'] = 'or drag and drop an image here';
$hl['OR_DRAG_THEM_HERE'] = 'or drag and drop files here';



$hl['PRODUCT_NAME'] = 'JS-LIBRARY';
$hl['PRODUCT_NAME_SPACER'] = ' | ';
$hl['HOME'] = 'Home';// Home page
$hl['PAGE_TITLE_TEST'] = 'Testing Page';
$hl['PAGE_TITLE_DRAG_SELECTION'] = 'Drag Selection';
?>