<?php
session_start();

$staticRoot = "pimg";

$_SESSION["hl"] = "en";
if( isset( $_GET["hl"] ) ){
    // set host-language
    $_SESSION["hl"] = $_GET["hl"];
    setcookie('hl', $_SESSION["hl"], time() + 157680000,'/'); //expires in 5 years
}else if( ! isset( $_COOKIE['hl'] ) ){
    setcookie('hl', $_SESSION["hl"], time() + 157680000,'/'); //expires in 5 years
}else{
    $_SESSION["hl"] = $_COOKIE["hl"];
}


include("hl/" . $_SESSION["hl"] . ".php");
?>