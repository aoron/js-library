<?php
header('Content-Type: application/json');
//header("HTTP/1.0 588 XXX XXXXX000");
?>{
  "code": 588,
  "message": "string",
  "result": {
    "guid": "string",
    "videoId": 0,
    "personGroupId": 0,
    "fRDSoftWareId": 0,
    "tagInfo": {
  "environment": {
    "personEnv": {
      "faceUrlBase": "http://192.168.11.174:8080/taggingStorage/PersonGroup/"
    },
    "clipEnv": {
      "faceUrlBase": "http://192.168.11.174:8080/taggingStorage/Person/"
    }
  },
  "tagClips": [
    {
      "person": {
        "guid": "08E93A77BE4C-4E78A3B9-4548-4456-D354",
        "name": "Elizabeth Olsen",
        "faceUrl": "a6/cc/19dfbfa19af1-c411d393-2861-11e6-a6cc/20160602093145410.png"
      },
      "clips": [
        {
          "beginTime": "00:12:06.100",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/a768e4b61865-94aadd16-23e4-11e6-8aec/20160527042534356.jpg"
        },
        {
          "beginTime": "00:12:06.101",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/a768e4b61865-94aadd16-23e4-11e6-8aec/20160527042534356.jpg"
        },
        {
          "beginTime": "00:12:06.102",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/a768e4b61865-94aadd16-23e4-11e6-8aec/20160527042534356.jpg"
        },
        {
          "beginTime": "00:12:06.103",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/a768e4b61865-94aadd16-23e4-11e6-8aec/20160527042534356.jpg"
        },
        {
          "beginTime": "00:12:06.104",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/a768e4b61865-94aadd16-23e4-11e6-8aec/20160527042534356.jpg"
        }
      ]
    },
    {
      "person": {
        "guid": "08E93A77BE4C-4E78A3B9-4548-4456-D355",
        "name": "Aussie Orsbon",
        "faceUrl": "9f/05/072f6f628c74-85ca4ffa-23e5-11e6-9f05/20160527043218627.jpg"
      },
      "clips": [
        {
          "beginTime": "00:12:06.100",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711262.jpg"
        },
        {
          "beginTime": "00:12:06.101",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711262.jpg"
        },
        {
          "beginTime": "00:12:06.102",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711262.jpg"
        },
        {
          "beginTime": "00:12:06.103",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711262.jpg"
        },
        {
          "beginTime": "00:12:06.104",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711262.jpg"
        }
      ]
    }
  ],
  "unknownClips": [
    {
      "group": {
        "id": "1",
        "name": "group 1"
      },
      "clips": [
        {
          "beginTime": "00:12:06.100",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.101",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.102",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.104",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.105",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.106",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.107",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.108",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.109",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.110",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.111",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.112",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.113",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        },
        {
          "beginTime": "00:12:06.114",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015710262.jpg"
        }
      ]
    },
    {
      "group": {
        "id": "2",
        "name": "group 2"
      },
      "clips": [
        {
          "beginTime": "00:12:06.100",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.101",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.102",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.104",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.105",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.106",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.107",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.108",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.109",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.110",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.111",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.112",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        },
        {
          "beginTime": "00:12:06.113",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160531015711259.jpg"
        }
      ]
    },
    {
      "group": {
        "id": "3",
        "name": "group 3"
      },
      "clips": [
        {
          "beginTime": "00:12:06.100",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.101",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.102",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.104",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.105",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.106",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.107",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.108",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.109",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.110",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.111",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.112",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        },
        {
          "beginTime": "00:12:06.113",
          "endTime": "00:15:38.156",
          "faceUrl": "8a/ec/573643ff6ef9-ad994f57-23e4-11e6-8aec/20160527043519221.jpg"
        }
      ]
    }
  ]
},
    "createDateTime": "string",
    "lastActionDateTime": "2016-06-03T08:16:50.997Z",
    "videoGuid": "15c513ab644a-9c8a6ff0-2a05-11e6-87e3",
    "personGroupGuid": "c12c9f0fac83-43e049f3-23f9-11e6-bda0",
    "status": "string",
    "OperationError": "string"
  }
}