<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include("lib/config.php");

include("inc/beforebody.php");
include("inc/masterheader.php");

echo "<div id=\"bd\"></div>";

include("inc/masterfooter.php");
include("inc/afterbody.php");
?>